<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deposits".
 *
 * @property int $id
 * @property int $agent_id
 * @property int $brand_id
 * @property int $account_id
 * @property string $deposit_date
 * @property float $amount
 * @property int|null $currency
 * @property int|null $payment_method
 *
 * @property Agents $agent
 * @property Clients $account
 */
class Deposits extends \yii\db\ActiveRecord
{
    public $department;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deposits';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agent_id', 'brand_id', 'deposit_date', 'amount'], 'required'],
            [['agent_id', 'brand_id', 'account_id', 'currency', 'payment_method', 'notif', 'type', 'user_id', 'wd_type'], 'integer'],
            [['deposit_date', 'department'], 'safe'],
            [['amount'], 'number'],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agents::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['account_id' => 'account_id']],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ( $this->type == '2' ) {
                $this->amount = abs($this->amount) * ( -1 );
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Creator'),
            'wd_type' => Yii::t('app', 'Withdrawal type'),
            'agent_id' => Yii::t('app', 'Agent'),
            'brand_id' => Yii::t('app', 'Brand'),
            'account_id' => Yii::t('app', 'Client Account ID'),
            'deposit_date' => Yii::t('app', 'Deposit Date'),
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'notif' => Yii::t('app', 'Sended notification'),
            'department' => Yii::t('app', 'Department'),
        ];
    }

    public static function getTotal($provider, $fieldName) {
        $total = 0;

        foreach ($provider as $item) {
            $total += $item[$fieldName];
        }
        if ( isset($item) ) {
            return Yii::$app->cc->getCurrencies($item['currency']) . number_format($total, 2);
        } else {
            return '0.00';
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Agent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agents::className(), ['id' => 'agent_id']);
    }

    /**
     * Gets query for [[Account]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Clients::className(), ['account_id' => 'account_id'])->andWhere(['clients.brand_id' => $this->brand_id]);
    }
}
