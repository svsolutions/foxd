<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "meta".
 *
 * @property int $id
 * @property int $item_id
 * @property string $meta_key
 * @property string $value
 * @property string $type
 */
class Meta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'meta_key', 'value', 'type'], 'required'],
            [['item_id'], 'integer'],
            [['meta_key'], 'string', 'max' => 256],
            [['type'], 'string', 'max' => 128],
        ];
    }

    public function getPage() {
        return $this->hasOne(Pages::className(), ['id' => 'meta_key']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'meta_key' => Yii::t('app', 'Meta Key'),
            'value' => Yii::t('app', 'Value'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
