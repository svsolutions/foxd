<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "access_tokens".
 *
 * @property int $id
 * @property string $access_token
 * @property int $user_id
 * @property string $valid_to
 */
class AccessTokens extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'access_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['access_token', 'user_id', 'valid_to'], 'required'],
            [['user_id'], 'integer'],
            [['valid_to'], 'safe'],
            [['access_token'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'user_id' => Yii::t('app', 'User ID'),
            'valid_to' => Yii::t('app', 'Valid To'),
        ];
    }
}
