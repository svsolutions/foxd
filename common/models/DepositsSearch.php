<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deposits;

/**
 * DepositsSearch represents the model behind the search form of `common\models\Deposits`.
 */
class DepositsSearch extends Deposits
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'agent_id', 'brand_id', 'account_id', 'currency', 'payment_method', 'notif', 'type', 'user_id', 'wd_type'], 'integer'],
            [['deposit_date', 'department'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deposits::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => [ 'id' => SORT_DESC ]]
        ]);

        $this->load($params);

        if ( isset($params['DepositsSearch']['agent_id']) && is_array($params['DepositsSearch']['agent_id']) ) {
            foreach ($params['DepositsSearch']['agent_id'] as $k => $value) {
                if ( $k == 0 ) {
                    $query->andWhere([
                        'agent_id' => $value
                    ]);
                } else {
                    $query->orWhere([
                        'agent_id' => $value
                    ]);
                }
            }
        } else {
            $query->andFilterWhere([
                'agent_id' => $this->agent_id
            ]);
        }

        if ( isset($params['from_date']) ) {
            $query->andFilterWhere(['>=', 'deposit_date', $params['from_date']]);
        }
        if ( isset($params['to_date']) ) {
            $query->andFilterWhere(['<=', 'deposit_date', $params['to_date']]);
        }

        if ( isset($params['DepositsSearch']['department']) ) {
            $query->joinWith('agent')->andFilterWhere(['dep_id' => $params['DepositsSearch']['department']]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'wd_type' => $this->wd_type,
            'deposits.brand_id' => $this->brand_id,
            'account_id' => $this->account_id,
            'deposit_date' => $this->deposit_date,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'payment_method' => $this->payment_method,
            'notif' => $this->notif,
        ]);

        return $dataProvider;
    }
}
