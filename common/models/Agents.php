<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "agents".
 *
 * @property int $id
 * @property int $dep_id
 * @property string $name
 * @property int|null $active
 *
 * @property Deposits[] $deposits
 */
class Agents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id', 'name'], 'required'],
            [['dep_id', 'active', 'brand_id'], 'integer'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dep_id' => Yii::t('app', 'Dep ID'),
            'brand_id' => Yii::t('app', 'Brand'),
            'name' => Yii::t('app', 'Name'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    public function getMonthTotal($nAgentID = '', $nBrandID = '', $nSecK = false)
    {
        $nTotal = Deposits::find()->andWhere(['brand_id' => $nBrandID]);
        if ( $nSecK ) {
            $nTotal = $nTotal->orWhere(['brand_id' => $nSecK]);
        }
        $nTotal = $nTotal->andWhere(['>=', 'deposit_date', date('Y-m-01')])->andWhere(['agent_id' => $nAgentID]);

        $nTotal = $nTotal->sum('amount');
// echo '<pre>'; print_r($nTotal); die;
        return empty($nTotal) ? '0.00' : $nTotal;
    }

    public function getMonthCount($nAgentID = '', $nBrandID = '', $nSecK = false)
    {
        $nTotal = Deposits::find()->andWhere(['brand_id' => $nBrandID]);
        if ( $nSecK ) {
            $nTotal = $nTotal->orWhere(['brand_id' => $nSecK]);
        }
        $nTotal = $nTotal->andWhere(['>=', 'deposit_date', date('Y-m-01')])->andWhere(['agent_id' => $nAgentID]);

        return $nTotal->count();
        // return Deposits::find()->andWhere(['agent_id' => $nAgentID, 'brand_id' => $nBrandID])->andWhere(['>=', 'deposit_date', date('Y-m-01')])->count();
    }

    /**
     * Gets query for [[Deposits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeposits()
    {
        return $this->hasMany(Deposits::className(), ['agent_id' => 'id']);
    }
}
