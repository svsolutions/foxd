<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property int $account_id
 * @property string $name
 * @property string|null $from_date
 * @property int|null $active
 *
 * @property Deposits[] $deposits
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['account_id', 'name'], 'required'],
            [['account_id', 'active', 'brand_id'], 'integer'],
            [['from_date'], 'safe'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'brand_id' => Yii::t('app', 'Brand'),
            'name' => Yii::t('app', 'Name'),
            'from_date' => Yii::t('app', 'From Date'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * Gets query for [[Deposits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeposits()
    {
        return $this->hasMany(Deposits::className(), ['account_id' => 'id']);
    }
}
