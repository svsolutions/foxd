<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;
 
class CommonComponent extends Component {
	public function getDepartments($nID = '') {
		$aData = [
			'1' => 'Retention',
			'2' => 'Conversion',
			'3' => 'Mini Desk'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		return $aData;
	}

	public function getWTypes($nID = '', $bNull = false) {
		$aData = [
			'1' => 'Withdrawal',
			'2' => 'Refund',
			'3' => 'Chargeback'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		if ( $bNull ) {
			return null;
		}

		return $aData;
	}

	public function getTypes($nID = '') {
		$aData = [
			'1' => 'Deposit',
			'2' => 'Withdrawal'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		return $aData;
	}

	public function getBrands($nID = '') {
		$aData = [
			'1' => 'LifecoinFX',
			'2' => 'Quantum',
			'3' => 'Referral - LifecoinFX',
			'4' => 'Referral - Quantum'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		return $aData;
	}

	public function getCurrencies($nID = '') {
		$aData = [
			'1' => '€',
			'2' => '$'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		return $aData;
	}

	public function getPaymentMethods($nID = '') {
		$aData = [
			'1' => 'Wire',
			'2' => 'BTC',
			'3' => 'CC - Bridgerpay',
			'4' => 'Coini Exchange',
			'5' => 'ETH',
			'6' => 'Xpate',
			'7' => 'Jubiter',
			'8' => 'BridgePay',
			'9' => 'Texcent',
			'10' => 'Safello',
			'11' => 'CC coindeck',
			'12' => 'Bitnomics',
			'13' => 'USDT',
			'14' => 'Doge',
			'15' => 'SecurePaymentGateway',
			'16' => 'MoonPay',
			'17' => 'Ibinex',
			'18' => 'BT/CX'
		];
		if ( !empty($nID) && isset($aData[$nID]) ) {
			return $aData[$nID];
		}

		return $aData;
	}
	
	public function getAgents($nID = '') {
		$model = \common\models\Agents::find();
		if ( !empty($nID) ) {
			$model = $model->andWhere(['id' => $nID])->one();
		} else {
			$model = $model->all();
		}
		return $model;
	}

	public function getClients($nID = '') {
		$model = \common\models\Clients::find();
		if ( !empty($nID) ) {
			$model = $model->andWhere(['account_id' => $nID])->one();
		} else {
			$model = $model->all();
		}
		return $model;
	}
}