<?php
return [
    'adminEmail' => 'asd@asd.asd',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'languages' => [
        '' => 'English',
    ],
    'image_sizes' => [
    ],
    'domain' => 'http://localhost:8080/dashboard/'
];
