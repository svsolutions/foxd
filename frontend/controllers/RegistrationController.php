<?php
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\RegistrationController as BaseRegistrationController;

use dektrium\user\Finder;

use common\models\Meta;
use common\models\User;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\components\AccessFilter;

class RegistrationController extends BaseRegistrationController {

    public function behaviors()
    {
        throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page doesn\'t exist.'));
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessFilter::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['register', 'resend', 'confirm'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['connect'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }
}