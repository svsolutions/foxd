<?php
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\SecurityController as BaseSequrityController;

use dektrium\user\Finder;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\components\AccessFilter;

use yii\helpers\Url;

class SecurityController extends BaseSequrityController {
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessFilter::className(),
                ],
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['auth', 'logout'], 'roles' => ['@']],
                    
                ],
            ],
        ];
    }


    /**
     * Displays the login page.
     *
     * @return string|Response
     */
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $pRequest = Yii::$app->request;

        /** @var LoginForm $model */
        $model = \Yii::createObject(\frontend\models\LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
       
        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            $strLang = Yii::$app->request->get('language');
            return $this->redirect(Url::base() . '/' . $strLang);
        }
        return $this->render('//security/login', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }
}