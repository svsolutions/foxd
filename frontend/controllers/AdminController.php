<?php
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\AdminController as BaseAdminController;

use dektrium\user\Finder;
use \yii\helpers\Url;

class AdminController extends BaseAdminController {
    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, Yii::createObject(Finder::className()), $config);
        $this->redirect(Url::to(['/site/error']));
    }
}