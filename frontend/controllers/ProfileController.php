<?php
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\ProfileController as BaseProfileController;

use dektrium\user\Finder;

class ProfileController extends BaseProfileController {
	public function __construct($id, $module, $config = []) {
		parent::__construct($id, $module, Yii::createObject(Finder::className()), $config);
	}

    /**
     * Shows user's profile.
     *
     * @param int $id
     *
     * @return \yii\web\Response
     */
    public function actionShow($id) {
        throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page doesn\'t exist.'));
    }

    
}