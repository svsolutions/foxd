<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\components\CoreController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Meta;
use common\models\Clients;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\components\AccessFilter;

/**
 * Site controller
 */
class SiteController extends CoreController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessFilter::className()
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'logout', 'stats'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                     [
                        'actions' => ['clients'],
                        'allow' => true,
                        'roles' => ['@', '?']
                    ],
                ]
            ]
        ];
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        $strLang = Yii::$app->request->get('language');
        Yii::$app->user->logout();
        Yii::$app->language = $strLang;

        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'language',
            'value' => $strLang
        ]));

        return $this->redirect(Url::base() . '/' . $strLang);
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function selectAction() {
        if ( Yii::$app->user->can('agent') ) {
            return $this->redirect(['/site/stats']);
        }
        return $this->render('action');
    }

    public function actionStats() {
        $this->layout = 'stats';
        $aTmp = explode(',', Yii::$app->user->identity->profile->name);
        if ( empty($aTmp) ) {
            echo 'Not set name';
            die;
        }
        $model = \common\models\Agents::find();
        foreach ( $aTmp as $strName ) {
            $model = $model->orWhere(['name' => trim($strName)]);
        }
        $model = $model->all();
        $nID = [];
        foreach ( $model as $pRow ) {
            $nID[] = $pRow['id'];
        }

        $aParams = Yii::$app->request->queryParams;
        $aParams['DepositsSearch']['agent_id'] = $nID;
        if ( empty($aParams['from_date']) ) {
            $aParams['from_date'] = $aParams['DepositsSearch']['from_date'] = date('Y-m-01');
        }
        
        $searchModel = new \common\models\DepositsSearch();
        $dataProvider = $searchModel->search($aParams);

        return $this->render('stats', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'aParams' => $aParams
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $this->view->params['homePage'] = true;
        $dep_id = Yii::$app->request->post('dep_id', false);
        if ( empty($dep_id) ) {
            $dep_id = Yii::$app->request->get('dep_id', false);;
        }
        if ( !$dep_id ) {
            return $this->selectAction();
            exit();
        }

        $model = \common\models\Deposits::find()->joinWith('agent', true, 'INNER JOIN')->andWhere(['type' => '1'])->andWhere(['dep_id' => $dep_id])->andWhere(['notif' => '0'])->one();

        if ( !empty($model) ) {
            $model->notif = '1';
            $model->update();
            $model = \common\models\Deposits::findOne($model->id);
        }

        $aData = [];
        $aAgents = \common\models\Agents::find()->andWhere(['active' => 1, 'dep_id' => $dep_id])->all();
        // if ( isset($_GET['d']) ) {
        //     // echo '<pre>'; print_r($aAgents); die;
        // }
        foreach ( Yii::$app->cc->getBrands() as $k => $strBrand ) {
            $nSecK = false;
            if ( $k == 3 || $k == 4 ) continue;

            if ( $k == 1 ) { // If LifecoinFX
                $nSecK = 3; // Add Referral - LifecoinFX
            } else if ( $k == 2 ) { // If Quantum
                $nSecK = 4; // Add Referral - Quantum
            }

            $aData[$k] = [
                'total' => 0,
                'brand' => $strBrand,
                'brand_id' => $k,
            ];
            foreach ( $aAgents as $pRow ) {
                if ( $dep_id == 1 ) {
                    $nTmp = $pRow->getMonthTotal($pRow->id, $k, $nSecK);
                    $aData[$k]['items'][intval($nTmp)  . '-' . $pRow->name] = [
                        'total' => $nTmp,
                        'agent' => $pRow,
                        'count' => $pRow->getMonthCount($pRow->id, $k, $nSecK)
                    ];
                } else {
                    $nTmp = $pRow->getMonthCount($pRow->id, $k, $nSecK);
                    $aData[$k]['items'][$nTmp . '-' . $pRow->name] = [
                        'total' => $nTmp,
                        'agent' => $pRow,
                        'count' => $pRow->getMonthCount($pRow->id, $k, $nSecK)
                    ];
                }

                $aData[$k]['total'] += $nTmp;
            }
        }

        if ( isset($aData[1]['items']) ) {
            arsort($aData[1]['items']);
        }

        if ( isset($aData[2]['items']) ) {
            arsort($aData[2]['items']);
        }

        $aLast = [];
        foreach ( Yii::$app->cc->getDepartments() as $k => $v ) {
            $aLast[$v] = \common\models\Deposits::find()->with(['agent'])->joinWith(['agent'])->andWhere(['type' => '1'])->andWhere(['dep_id' => $k])->orderBy(['id' => SORT_DESC])->one();

            if ( $k == 1 ) {
                $aStats[$k] = [
                    'dep' => $v,
                    'total' => \common\models\Deposits::find()->with(['agent'])->joinWith(['agent'])->andWhere(['dep_id' => $k])->andWhere(['>=', 'deposit_date', date('Y-m-d')])->sum('amount')
                ];
            } else {
                $aStats[$k] = [
                    'dep' => $v,
                    'total' => \common\models\Deposits::find()->with(['agent'])->joinWith(['agent'])->andWhere(['dep_id' => $k])->andWhere(['>=', 'deposit_date', date('Y-m-d')])->count()
                ];
            }
        }

        // $aStats[1]['total'] = 105050;
        // $aStats[2]['total'] = 50;
        // if ( isset($_GET['d']) ) {
        //     echo '<pre>'; print_r($aData[2]); die;
        // }
        return $this->render('index', [
            'model' => $model,
            'aLast' => $aLast,
            'aData' => $aData,
            'aStats' => $aStats,
            'dep_id' => $dep_id
        ]);
    }

    /**
     * Displays error page.
     *
     * @return mixed
     */
    public function actionError() {
        return $this->render('error');
    }

    public function actionClients($strCurrentBrand = 'lifecoinfex') {

        $aData = [];
        $bSkipAPI = false;
        $bDebug = true;

        foreach ( Yii::$app->params['api'] as $k => $aSource ) { // api_test
            if ( $k != $strCurrentBrand ) continue;
            $nPage = 1;
            $nTotal = 2;

            if ( $bDebug )
                echo 'API: ' . $k . '<br />';

            for ( $i = $nPage; $i <= $nTotal; $i++ ) {
                if ( $bDebug )
                    echo 'Page: ' . $i . ' - ' . $nTotal . '<br />';
                if ( $bSkipAPI ) {
                    $bSkipAPI = false;
                    break;
                }
                $curl = curl_init();
                // $nAfterID = '0';
                // $aClients = Meta::find()->andWhere(['type' => 'last_id'])->andWhere(['meta_key' => $k])->one();
                // if ( !empty($aClients) ) {
                //     $nAfterID = intval($aClients->value) + 1;
                // } else {
                //     $aClients = new Meta();
                //     $aClients->meta_key = $k;
                //     $aClients->item_id = 2;
                //     $aClients->value = '0';
                //     $aClients->type = 'last_id';
                //     $aClients->save();
                //     $nAfterID = intval($aClients->value);
                // }

                $nAfterID = 1;

                echo 'https://' . $aSource['url'] . '/api/v6/terms/Traders?token=' . $aSource['token'] . '&page=' . $i . '&conditions%5B0%5D%5Bkey%5D=customers.id&conditions%5B0%5D%5Boperator%5D=%3E&conditions%5B0%5D%5Bvalue%5D=' . $nAfterID . '&conditions%5B0%5D%5Bconfig%5D=and';
                // echo '<pre>'; print_r($_SERVER);

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://' . $aSource['url'] . '/api/v6/terms/Traders?token=' . $aSource['token'] . '&page=' . $i . '&conditions%5B0%5D%5Bkey%5D=customers.id&conditions%5B0%5D%5Boperator%5D=%3E&conditions%5B0%5D%5Bvalue%5D=' . $nAfterID . '&conditions%5B0%5D%5Bconfig%5D=and',
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_RETURNTRANSFER => true,
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                if ( !empty($response) ) {
                    $aData = json_decode($response);
                }
                echo '<pre>'; print_r($response); echo '</pre>';

                $nTotal = $aData->meta->pagination->total_pages;

                if ( isset($aData->data) && !empty($aData->data) ) {
                    if ( $bDebug ) {
                        echo 'Data Exist: ' . $aSource['brand_id'] . '<br />';
                        // echo '<pre>'; print_r($aData->data); echo '</pre>';
                    }
                    $aData->data = array_reverse($aData->data);
                    // echo '<pre>'; print_r($aData->data); die;
                    foreach ( $aData->data as $pRow ) {
                        $pRow = (array) $pRow;
                        $model = Clients::find()->andWhere(['account_id' => $pRow['Trader.ID']])->andWhere(['brand_id' => $aSource['brand_id']])->one();
                        if ( empty( $model ) ) {
                            echo 'Create: ' . $pRow['Trader.First Name'] . ' ' . $pRow['Trader.Last Name'] . '<br />';
                            $model = new Clients;
                            $model->brand_id = $aSource['brand_id'];
                            $model->account_id = $pRow['Trader.ID'];
                            $model->name = $pRow['Trader.First Name'] . ' ' . $pRow['Trader.Last Name'];
                            $model->from_date = date('Y-m-d H:i:s');
                            $model->active = 1;
                            $model->save();
                            if ( $bDebug ) {
                                echo $k . ' - Add: ' . $model->name . '<br />';
                            }
                            // $aClients = Meta::find()->andWhere(['type' => 'last_id'])->andWhere(['meta_key' => $k])->one();
                            // $aClients->value = (string) $pRow['Trader.ID'];    
                            // $aClients->update();
                        } else {
                            
                            if ( $bDebug ) {
                                echo 'Exist: ' . $pRow['Trader.First Name'] . ' ' . $pRow['Trader.Last Name'] . ' - ' . $model->account_id . '<br />';
                            }
                            $nTotal = 1;
                            $bSkipAPI = true;
                            // break;
                        }
                    }
                } else {
                    if ( $bDebug ) {
                        echo 'Not Exist: ' . $aSource['brand_id'] . '<br />';
                    }
                }
            }
            echo '<br /><br />';
        }
        echo 'Finish ...'; die;
    }

}
