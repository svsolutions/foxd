<?php
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\SettingsController as BaseSettingsController;

use dektrium\user\Finder;

use yii\filters\VerbFilter;

class SettingsController extends BaseSettingsController {
	public function __construct($id, $module, $config = []) {
		parent::__construct($id, $module, Yii::createObject(Finder::className()), $config);
        throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page doesn\'t exist.'));
	}
}




