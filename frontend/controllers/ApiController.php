<?php
namespace frontend\controllers;

use \Yii;
use yii\rest\ActiveController;
use yii\web\Controller;

use common\models\AccessTokens;
use common\models\Clients;

class ApiController extends Controller {

	private $request;
	private $ATExpired = 8; // 8 hours
	private $aUsers = [
		'1' => [
			'u' => 'user1000',
			'p' => '1qazxcvb@#$%'
		]
	];

    public function init() {
	    parent::init();
	    $this->enableCsrfValidation = false;
	    $this->request = \Yii::$app->request;

	    Yii::$app->user->enableSession = false;
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	}
  	
  	/*
  	 * Login user with username and password
  	 */
  	public function actionLogin() {
  		if ( $this->request->isPost) {
  			$user = $this->request->post('u', false);
  			$pass = $this->request->post('p', false);
  			if ( $user && $pass ) {
  				foreach ( $this->aUsers as $k => $aRow ) {
  					if ( $aRow['u'] == $user && $aRow['p'] == $pass ) {

  						$model = AccessTokens::find()->andWhere(['user_id' => $k])->one();
  						if ( empty($model) ) {
							$model = new AccessTokens();
							$model->user_id = $k;
  						}
  						$model->access_token = md5('!@#$%^&*(' . time() . $k);
  						$model->valid_to = date('Y-m-d H:i:s', strtotime('+' . $this->ATExpired . ' hours'));
  						$model->save();
  						$this->setHeader(200);
  						return['access_token' => $model->access_token, 'valid_to' => $model->valid_to];
  					}
  				}
	  			$this->setHeader(403);
	  			return['status' => 403, 'msg' => 'Wrong username or password'];
  			}
  			$this->setHeader(403);
  			return['status' => 403, 'msg' => $this->_getStatusCodeMessage(403)];
  		}
  		$this->setHeader(501);
  		return['status' => 501, 'msg' => $this->_getStatusCodeMessage(501)];
  	}

  	/*
  	 * Client
  	 */
  	public function actionClient() {
  		$accessToken = $this->request->post('access_token', false);
  		if ( $accessToken ) {
  			if ( !( $accessToken = $this->checkAccessToken($accessToken) ) ) {
	  			$this->setHeader(403);
	  			return['status' => 403, 'msg' => 'Wrong or expired access token.'];
	  		}
		} else {
			$this->setHeader(403);
			return['status' => 403, 'msg' => 'Missing access token.'];
		}


  		if ( $this->request->isGet ) {
			$model = Clients::find()->all();
			$aData = [];
			foreach ( $model as $pRow ) $aData[] = $pRow;

			return ['data' => $aData, 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to, 'status' => 200];
  			
  		} else if ( $this->request->isPost ) {
  			if ( $nClientId = $this->request->post('id', false) ) {
  				$model = Clients::findOne($nClientId);
  				if ( empty($model) ) {
  					$this->setHeader(400);
					return['status' => 400, 'msg' => 'Client with id \'' . $nClientId . '\' not found.', 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to];
  				}
  			} else {
  				$model = new Clients;
  				$model->from_date = $this->request->post('from_date', date('Y-m-d H:i:s'));
  			}

			if ( !( $strName = $this->request->post('name', false) ) ) {
				$this->setHeader(400);
				return['status' => 400, 'msg' => 'Missing field \'name\'.', 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to];
			}

			if ( !( $nAccountId = $this->request->post('account_id', false) ) ) {
				$this->setHeader(400);
				return['status' => 400, 'msg' => 'Missing field \'account_id\'.', 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to];
			}


  			$model->account_id = $nAccountId;
  			$model->name = $strName;
  			$model->active = $this->request->post('active', '1');
  			if ( $model->save() ) {
  				$this->setHeader(200);
  				return['status' => 200, 'data' => $model->getAttributes(), 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to];
  			}

  		}
  		$this->setHeader(501);
  		return['status' => 501, 'msg' => $this->_getStatusCodeMessage(501), 'access_token' => $accessToken->access_token, 'valid_to' => $accessToken->valid_to];
  	}

  	private function checkAccessToken($accessToken = '') {
  		if ( !empty($accessToken) ) {
  			$model = AccessTokens::find()->andWhere(['access_token' => $accessToken])->andWhere(['>=', 'valid_to', date('Y-m-d H:i:s')])->one();
  			if ( !empty($model) ) {
  				$model->access_token = md5('!@#$%^&*(' . time() . $model->user_id);
  				$model->valid_to = date('Y-m-d H:i:s', strtotime('+' . $this->ATExpired . ' hours'));
  				$model->update();
  				return $model;
  			}
  			return false;
  		}
  		return false;
  	}

	private function setHeader($status) {
		Yii::$app->response->statusCode = $status;
		Yii::$app->response->statusText = $this->_getStatusCodeMessage($status);
		Yii::$app->response->headers->set('Content-type', 'application/json; charset=utf-8');
	}

	private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			403 => 'Forbidden',
			404 => 'Not Found!',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
}