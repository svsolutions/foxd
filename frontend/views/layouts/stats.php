<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\StatsAsset;
use common\widgets\Alert;
use yii\web\View;

StatsAsset::register($this);

$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;


$this->registerMetaTag(['content' => Yii::$app->params['domain'] . Yii::$app->request->url, 'property' => 'og:url']);
if (!($controller->id == 'news' && $controller->action->id == 'view')) {
    $this->registerMetaTag(['content' => Yii::$app->params['domain'] . Url::to(['/img/e5.jpg']), 'property' => 'og:image']);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>

        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <?= Html::csrfMetaTags() ?>
        <?php
        $this->registerJs(
                "
                var bReload = true;
                var baseUrl = '" . Url::base() . "';
                var strUrl = '//" . $_SERVER['HTTP_HOST'] . "/'", View::POS_HEAD, 'helpers'
        );
        ?>

        <title><?= $this->title ?></title>

        
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body class="<?= $isHome ? 'home' : ''; ?>">

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Sign in', 'url' => ['/site/login']];
    } else {
        
        if ( Yii::$app->user->can('admin') ) {
            $menuItems[] = ['label' => 'Deposits & Withdrawals', 'url' => ['/deposits']];
            $menuItems[] = ['label' => 'Agents', 'url' => ['/agents']];
            $menuItems[] = ['label' => 'Clients', 'url' => ['/clients']];
            $menuItems[] = ['label' => 'Users', 'url' => ['/user/admin/index']];
        }
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

        <div class="container" style="margin-top: 100px;">
        <?= $content; ?>
        </div>
            
    </body>
    <?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
