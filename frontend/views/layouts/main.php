<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\web\View;

AppAsset::register($this);

$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;


$this->registerMetaTag(['content' => Yii::$app->params['domain'] . Yii::$app->request->url, 'property' => 'og:url']);
if (!($controller->id == 'news' && $controller->action->id == 'view')) {
    $this->registerMetaTag(['content' => Yii::$app->params['domain'] . Url::to(['/img/e5.jpg']), 'property' => 'og:image']);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="wide">
    <head>

        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <?= Html::csrfMetaTags() ?>
        <?php
        $this->registerJs(
                "
                var bReload = true;
                var baseUrl = '" . Url::base() . "';
                var strUrl = '//" . $_SERVER['HTTP_HOST'] . "/'", View::POS_HEAD, 'helpers'
        );
        ?>

        <title><?= $this->title ?></title>

        
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body class="<?= $isHome ? 'home' : ''; ?>">
            <?= $this->render('_header'); ?>
            
            <?= $content; ?>
            
            <?= $this->render('_footer'); ?>
    </body>
    <?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
