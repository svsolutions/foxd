<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Login');

$this->registerCssFile("https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css", [
], 'css-bootstrap');

?>

<section class="wrapper section-login-form">
    <div class="container">
        <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                    'class' => 'login-form form',
                    'style' => 'margin-top: 10%;'
                ],
                'fieldConfig' => [
                    'template' => '{label}{input}',
                    'labelOptions' => [
                        'class' => 'label'
                    ],
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'enableClientScript' => false,
                'validateOnChange' => false,
            ]) ?>

            <?php if ($model->hasErrors()) : ?>
            <div class="alert alert-danger">
                <?= $form->errorSummary($model); ?>
            </div>
            <?php endif; ?>
            
            <div class="login-form-area form-area">
                <div class="row">
                    <div class="col-6 offset-3">
                        <div class="form-field">
                            <?= $form->field($model, 'login', ['inputOptions' => ['class' => 'input-text form-control js-input ' . ( !empty($model->username) ? 'not-empty' : '')], 'options' => ['class' => 'form-field']])->error(false) ?>
                        </div>
                    </div>
                    <div class="col-6 offset-3 mt-4">
                        <div class="form-field">
                            <?= $form->field(
                                $model,
                                'password',
                                ['inputOptions' => ['class' => 'form-control input-text', 'tabindex' => '2']])
                                ->passwordInput()
                                ->label(
                                    Yii::t('user', 'Password')
                                ) ?>

                        </div>
                    </div>
                    
                    <div class="col-6 offset-3 text-center">
                        <?= Html::submitButton(Yii::t('user', 'Login'), ['class' => 'btn btn-primary col-6 offset-3 mt-3']) ?>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>


