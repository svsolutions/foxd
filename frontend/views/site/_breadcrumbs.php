<?php

use \yii\helpers\Url;
use \yii\helpers\Html;

$aItems = isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
?>
 
<section class="header-page block-top-content <?= isset($strClasses) ? $strClasses : '' ?> noprint" style="<?= isset($strImg) && !empty($strImg) ? 'background: url(' . Url::to([$strImg]) . ') no-repeat center center; background-size: cover;' : ''; ?>">
    <div class="background-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-group">
                    <h1 class="section-title"><?= $this->title; ?></h1>
                    <h3><?= isset($strSecondTitle) && !empty($strSecondTitle) ? $strSecondTitle : ''; ?></h3>
                </div>
                <ul class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="<?= Url::to(['/']) ?>"><?= Yii::t('app', 'Home'); ?></a></li>
                    <?php if (isset($aItems) && !empty($aItems) && is_array($aItems)) : ?>
                        <?php foreach ($aItems as $k => $aItem) : ?>
                            <?php if (!isset($aItem['url'])) continue; ?>
                            <li class="breadcrumb-item"><a href="<?= Url::to($aItem['url']); ?>"><?= ucfirst($aItem['label']) ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <li class="breadcrumb-item active"><?= ucfirst($this->title) ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="shape-bottom">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none"> 
            <path class="shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
                  c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
                  c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"></path>
        </svg>
    </div>
</section>

