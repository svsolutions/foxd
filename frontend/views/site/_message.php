<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('app', 'System message');
?>

<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
			<?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
			    <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
			        <?= Alert::widget([
			            'options' => ['class' => 'alert-dismissible alert-' . $type],
			            'body' => $message
			        ]) ?>
			    <?php endif ?>
			<?php endforeach ?>
				<h1><?= $title; ?></h1>
				<?= Html::a(Yii::t('app', 'Начало'), ['/'], ['class' => 'button button-primary']); ?>
			</div>
		</div>
	</div>
</section>