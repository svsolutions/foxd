<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DepositsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'All Deposits & Withdrawals from {0}', [Yii::$app->user->identity->profile->name]);
$this->params['breadcrumbs'][] = $this->title;

$fromDate = $aParams['from_date'];
$toDate = Yii::$app->request->get('to_date');

?>
<div class="deposits-index">

    <p class="h2"><?= Html::encode($this->title) ?></p>

    <?php Pjax::begin(); ?>
    <h3 class="alert alert-info text-center fixed-left">
        Total: <?= \common\models\Deposits::getTotal($dataProvider->query->all(), 'amount'); ?>
    </h3>
    <?php // $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [

            [
                'attribute' => 'brand_id',
                'value' => function ($data) {
                    return Yii::$app->cc->getBrands($data->brand_id);
                },
                'filter' => Yii::$app->cc->getBrands()
            ],
            [
                'attribute' => 'account_id',
                'value' => function ($data) {
                    return $data->account->name;
                },
                'filter' => ArrayHelper::map(Yii::$app->cc->getClients(), 'id', 'name')
            ],
            [
                'attribute' => 'deposit_date',
                // 'format' => 'datetime',
                'filter' => '<div class="row"><div class="col-md-6"><input type="date" value="' . $fromDate . '" name="from_date" class="form-control" /></div><div class="col-md-6"><input type="date" value="' . $toDate . '" name="to_date" class="form-control" /></div></div>'
            ],
            [
                'attribute' => 'amount',
                'value' => function ($data) {
                    return Yii::$app->cc->getCurrencies($data->currency) . number_format(abs($data->amount), 2);
                },
                'footer' => \common\models\Deposits::getTotal($dataProvider->query->all(), 'amount'),  
            ],
            [
                'attribute' => 'payment_method',
                'value' => function ($data) {
                    return Yii::$app->cc->getPaymentMethods($data->payment_method);
                },
                'filter' => Yii::$app->cc->getPaymentMethods()
            ],
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    return Yii::$app->cc->getTypes($data->type);
                },
                'filter' => Yii::$app->cc->getTypes()
            ],
            [
                'attribute' => 'wd_type',
                'value' => function ($data) {
                    return Yii::$app->cc->getWTypes($data->wd_type, true);
                },
                'filter' => Yii::$app->cc->getWTypes()
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
