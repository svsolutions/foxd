<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = Yii::t('user', 'Select action');

?>
<a href="<?= Url::to(['/site/logout']) ?>" class="btn btn-primary" style="margin-top: 0;">Logout</a>
<?php Pjax::begin(['id' => 'id-pjax']); ?>

<?php ActiveForm::begin([ 'method' => 'POST', 'options' => [ 'data-pjax' => true, 'class' => 'container-fluid h-100' ]]); ?>

  <div class="row justify-content-center align-items-center h-100">
    <div class="col-6">
        <h2 class="text-center color-white">Open deposit panel</h2>
        <a href="<?= Url::to(['/admin']); ?>" class="btn btn-primary mt-4">Show deposits</a>
    </div>

    <div class="col-6 text-center">
        <h2 class="text-center color-white mb-5">Show leaderboard</h2>
        <div class="row justify-content-center">
        <?php foreach ( Yii::$app->cc->getDepartments() as $k => $v ) : ?>
        <label class="form-group btn btn-secondary color-white ml-2 mr-2">
            <input type="radio" name="dep_id" value="<?= $k; ?>" />&nbsp; <?= $v; ?> 
        </label>
        <?php endforeach; ?>
        </div>
        <input type="submit" class="btn btn-primary" value="Show" />
    </div>
  </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>