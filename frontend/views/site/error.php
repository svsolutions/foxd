<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Error');
?>

<?= $this->render('//site/_breadcrumbs'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 p-t-50">
            <article class="page-404 p-t-50 p-b-50">
                <div>
                    <h1>404</h1>
                    <h2><?= Yii::t('app', 'THIS PAGE IS NOT FOUND'); ?></h2>
                    <p><?= $name; ?></p>
                    <a href="<?= Url::to(['/']); ?>" class="btn btn-slider-black"><?= Yii::t('app', 'BACK TO HOME'); ?></a>
                </div>
            </article>
        </div>
    </div>
</div>