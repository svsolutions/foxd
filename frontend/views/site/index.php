<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = Yii::$app->name;

?>

<?php Pjax::begin(['id'=>'id-pjax']); ?>

<section class="block-text" id="run-loop" data-id="<?= $dep_id?>">
    <div class="container">
        <div class="row">
        <?php foreach ( $aData as $pRow ) : ?>
        	<div class="col-4">
                <p class="c-red"><?= $pRow['brand']; ?></p>
        		<table>
			<?php foreach ( $pRow['items'] as $k => $pTmp ) : ?>
                <?php if ( $dep_id == '1' ) : ?>
                    <?php if ( $pTmp['total'] == '0.00' ) continue; ?>
					<tr><td align="left"><?= $pTmp['agent']['name']; ?></td><td align="right">€ <?= $pTmp['total'] ?></td></tr>
                <?php else : ?>
                    <?php if ( $pTmp['count'] == 0 ) continue; ?>
                    <tr><td align="center"><?= $pTmp['count'] ?></td><td align="center"><?= $pTmp['agent']['name'] ?></td></tr>
                <?php endif; ?>
			<?php endforeach; ?>
				</table>
			</div>
		<?php endforeach; ?>

            <div class="col-4">
            	<?php foreach ( $aLast as $k => $v ) : if ( empty($v) ) continue; ?>
                <div class="col-6">
                    <ul>
                        <li>Last <?= $k; ?> Deposit</li>
                    </ul>
                </div>
                <div class="col-6 mb-4">
                    <ul class="font-light">
                        <li><?= $v->agent->name; ?></li>
                        <li><?= $v->amount; ?> EUR</li>
                        <li><?= date('d/m/Y H:i', strtotime($v->deposit_date)); ?></li>
                    </ul>
                </div>
            	<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="block-speedometer">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="progress" id="prog_1">
                    <h2>RETENTION</h2>
                    <div class="barOverflow">
                        <div class="bar-wrap"></div>
                        <div class="speedometer" >
                            <svg class="js-guage-svg guage-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 327 205.4">
                            <path d="M60.5 127.9l-17.9-3.8c-.1.6-.3 1.3-.4 1.9l17.9 3.8c.1-.6.3-1.3.4-1.9z"></path>
                            <path d="M63.2 118.3l-17.4-5.7c-.2.6-.4 1.2-.6 1.9l17.4 5.7c.2-.7.4-1.3.6-1.9z"></path>
                            <path d="M66.9 108.9l-16.7-7.5c-.3.6-.6 1.2-.8 1.8l16.7 7.5c.2-.6.5-1.2.8-1.8z"></path>
                            <path d="M71.5 100.1l-15.9-9.2c-.3.6-.7 1.1-1 1.7l15.9 9.2c.4-.6.7-1.2 1-1.7z"></path>
                            <path d="M77.1 91.7L62.2 81c-.4.5-.8 1.1-1.1 1.6l14.8 10.8c.4-.6.8-1.1 1.2-1.7z"></path>
                            <path d="M83.5 84L69.8 71.8c-.4.5-.9 1-1.3 1.5l13.6 12.3c.5-.6.9-1.1 1.4-1.6z"></path>
                            <path d="M90.6 77L78.3 63.4c-.5.4-1 .9-1.5 1.3l12.3 13.6c.5-.4 1-.8 1.5-1.3z"></path>
                            <path d="M98.5 70.8L87.7 56c-.5.4-1.1.8-1.6 1.1L96.8 72c.6-.4 1.1-.8 1.7-1.2z"></path>
                            <path d="M106.9 65.5l-9.2-15.9c-.6.3-1.1.7-1.7 1l9.2 15.9c.5-.4 1.1-.7 1.7-1z"></path>
                            <path d="M115.8 61l-7.5-16.7c-.6.3-1.2.5-1.8.8l7.5 16.7c.6-.3 1.2-.5 1.8-.8z"></path>
                            <path d="M125.2 57.5l-5.7-17.4c-.6.2-1.3.4-1.9.6l5.7 17.4c.7-.2 1.3-.4 1.9-.6z"></path>
                            <path d="M134.9 55l-3.8-17.9c-.7.1-1.3.3-1.9.4l3.8 17.9c.6-.1 1.3-.2 1.9-.4z"></path>
                            <path d="M144.8 53.6l-1.9-18.2c-.7.1-1.3.1-2 .2l1.9 18.2c.7-.1 1.4-.2 2-.2z"></path>
                            <path d="M153.8 53.2h1V34.8h-2v18.3c.4.1.7.1 1 .1z"></path>
                            <path d="M164.8 53.8l1.9-18.2c-.7-.1-1.3-.2-2-.2l-1.9 18.2c.7 0 1.3.1 2 .2z"></path>
                            <path d="M174.7 55.5l3.8-17.9c-.6-.1-1.3-.3-1.9-.4L172.7 55c.7.2 1.3.3 2 .5z"></path>
                            <path d="M184.3 58.1l5.7-17.4c-.6-.2-1.2-.4-1.9-.6l-5.7 17.4c.6.2 1.3.4 1.9.6z"></path>
                            <path d="M193.6 61.8l7.5-16.7c-.6-.3-1.2-.6-1.8-.8L191.8 61c.6.3 1.2.6 1.8.8z"></path>
                            <path d="M202.5 66.5l9.2-15.9c-.6-.3-1.1-.7-1.7-1l-9.2 15.9c.5.3 1.1.6 1.7 1z"></path>
                            <path d="M210.8 72l10.8-14.8c-.5-.4-1.1-.8-1.6-1.1l-10.8 14.8c.5.3 1.1.7 1.6 1.1z"></path>
                            <path d="M218.5 78.4l12.3-13.6c-.5-.4-1-.9-1.5-1.3L217 77.1c.5.4 1 .8 1.5 1.3z"></path>
                            <path d="M225.5 85.5l13.6-12.3c-.4-.5-.9-1-1.3-1.5L224.2 84c.4.5.9 1 1.3 1.5z"></path>
                            <path d="M231.7 93.4l14.8-10.8c-.4-.5-.8-1.1-1.1-1.6l-14.8 10.8c.4.5.7 1 1.1 1.6z"></path>
                            <path d="M237.1 101.8l15.9-9.2c-.3-.6-.7-1.1-1-1.7l-15.9 9.2c.3.6.7 1.1 1 1.7z"></path>
                            <path d="M241.5 110.8l16.7-7.5c-.3-.6-.5-1.2-.8-1.8l-16.7 7.5c.3.6.6 1.2.8 1.8z"></path>
                            <path d="M245 120.1l17.4-5.7c-.2-.6-.4-1.2-.6-1.9l-17.4 5.7c.2.7.4 1.3.6 1.9z"></path>
                            <path d="M247.5 129.8l17.9-3.8c-.1-.7-.3-1.3-.4-1.9l-17.9 3.8c.1.6.3 1.3.4 1.9z"></path>
                            </svg>
                        </div>
                        <div id="nums_1" class="nums">
                        	<?php if ( $aStats[1]['total'] < 50000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">10K</div>
                            <div class="num">20K</div>
                            <div class="num">30K</div>
                            <div class="num">40K</div>
                            <div class="num">50K</div>
                            <?php elseif ( $aStats[1]['total'] < 100000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">20K</div>
                            <div class="num">40K</div>
                            <div class="num">60K</div>
                            <div class="num">80K</div>
                            <div class="num">100K</div>
                            <?php elseif ( $aStats[1]['total'] < 200000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">40K</div>
                            <div class="num">80K</div>
                            <div class="num">120K</div>
                            <div class="num">160K</div>
                            <div class="num">200K</div>
                            <?php elseif ( $aStats[1]['total'] < 300000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">60K</div>
                            <div class="num">120K</div>
                            <div class="num">180K</div>
                            <div class="num">240K</div>
                            <div class="num">300K</div>
                            <?php elseif ( $aStats[1]['total'] < 400000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">80K</div>
                            <div class="num">160K</div>
                            <div class="num">240K</div>
                            <div class="num">320K</div>
                            <div class="num">400K</div>
                            <?php elseif ( $aStats[1]['total'] < 500000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">100K</div>
                            <div class="num">200K</div>
                            <div class="num">300K</div>
                            <div class="num">400K</div>
                            <div class="num">500K</div>
                            <?php elseif ( $aStats[1]['total'] < 600000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">120K</div>
                            <div class="num">240K</div>
                            <div class="num">360K</div>
                            <div class="num">480K</div>
                            <div class="num">600K</div>
                            <?php elseif ( $aStats[1]['total'] < 700000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">140K</div>
                            <div class="num">280K</div>
                            <div class="num">420K</div>
                            <div class="num">560K</div>
                            <div class="num">700K</div>
                            <?php elseif ( $aStats[1]['total'] < 800000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">140K</div>
                            <div class="num">280K</div>
                            <div class="num">420K</div>
                            <div class="num">560K</div>
                            <div class="num">800K</div>
                            <?php elseif ( $aStats[1]['total'] < 900000 ) : ?>
                            <div class="num">0</div>
                            <div class="num">240K</div>
                            <div class="num">380K</div>
                            <div class="num">520K</div>
                            <div class="num">760К</div>
                            <div class="num">900K</div>
                            <?php else : ?>
                            <div class="num">0</div>
                            <div class="num">2000K</div>
                            <div class="num">4000K</div>
                            <div class="num">6000K</div>
                            <div class="num">8000K</div>
                            <div class="num">10000K</div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <span class="perc total-price"><?= number_format($aStats[1]['total'], 2); ?></span><br>EUR
                </div>
            </div>
            <div class="col-6">
                <div class="progress" id="prog_2">
                    <h2 style="color:#ff9800">CONVERSION</h2>
                    <div class="barOverflow">
                        <div class="bar-wrap"></div>
                        <div class="speedometer" >
                            <svg class="js-guage-svg guage-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 327 205.4">
                            <path d="M60.5 127.9l-17.9-3.8c-.1.6-.3 1.3-.4 1.9l17.9 3.8c.1-.6.3-1.3.4-1.9z"></path>
                            <path d="M63.2 118.3l-17.4-5.7c-.2.6-.4 1.2-.6 1.9l17.4 5.7c.2-.7.4-1.3.6-1.9z"></path>
                            <path d="M66.9 108.9l-16.7-7.5c-.3.6-.6 1.2-.8 1.8l16.7 7.5c.2-.6.5-1.2.8-1.8z"></path>
                            <path d="M71.5 100.1l-15.9-9.2c-.3.6-.7 1.1-1 1.7l15.9 9.2c.4-.6.7-1.2 1-1.7z"></path>
                            <path d="M77.1 91.7L62.2 81c-.4.5-.8 1.1-1.1 1.6l14.8 10.8c.4-.6.8-1.1 1.2-1.7z"></path>
                            <path d="M83.5 84L69.8 71.8c-.4.5-.9 1-1.3 1.5l13.6 12.3c.5-.6.9-1.1 1.4-1.6z"></path>
                            <path d="M90.6 77L78.3 63.4c-.5.4-1 .9-1.5 1.3l12.3 13.6c.5-.4 1-.8 1.5-1.3z"></path>
                            <path d="M98.5 70.8L87.7 56c-.5.4-1.1.8-1.6 1.1L96.8 72c.6-.4 1.1-.8 1.7-1.2z"></path>
                            <path d="M106.9 65.5l-9.2-15.9c-.6.3-1.1.7-1.7 1l9.2 15.9c.5-.4 1.1-.7 1.7-1z"></path>
                            <path d="M115.8 61l-7.5-16.7c-.6.3-1.2.5-1.8.8l7.5 16.7c.6-.3 1.2-.5 1.8-.8z"></path>
                            <path d="M125.2 57.5l-5.7-17.4c-.6.2-1.3.4-1.9.6l5.7 17.4c.7-.2 1.3-.4 1.9-.6z"></path>
                            <path d="M134.9 55l-3.8-17.9c-.7.1-1.3.3-1.9.4l3.8 17.9c.6-.1 1.3-.2 1.9-.4z"></path>
                            <path d="M144.8 53.6l-1.9-18.2c-.7.1-1.3.1-2 .2l1.9 18.2c.7-.1 1.4-.2 2-.2z"></path>
                            <path d="M153.8 53.2h1V34.8h-2v18.3c.4.1.7.1 1 .1z"></path>
                            <path d="M164.8 53.8l1.9-18.2c-.7-.1-1.3-.2-2-.2l-1.9 18.2c.7 0 1.3.1 2 .2z"></path>
                            <path d="M174.7 55.5l3.8-17.9c-.6-.1-1.3-.3-1.9-.4L172.7 55c.7.2 1.3.3 2 .5z"></path>
                            <path d="M184.3 58.1l5.7-17.4c-.6-.2-1.2-.4-1.9-.6l-5.7 17.4c.6.2 1.3.4 1.9.6z"></path>
                            <path d="M193.6 61.8l7.5-16.7c-.6-.3-1.2-.6-1.8-.8L191.8 61c.6.3 1.2.6 1.8.8z"></path>
                            <path d="M202.5 66.5l9.2-15.9c-.6-.3-1.1-.7-1.7-1l-9.2 15.9c.5.3 1.1.6 1.7 1z"></path>
                            <path d="M210.8 72l10.8-14.8c-.5-.4-1.1-.8-1.6-1.1l-10.8 14.8c.5.3 1.1.7 1.6 1.1z"></path>
                            <path d="M218.5 78.4l12.3-13.6c-.5-.4-1-.9-1.5-1.3L217 77.1c.5.4 1 .8 1.5 1.3z"></path>
                            <path d="M225.5 85.5l13.6-12.3c-.4-.5-.9-1-1.3-1.5L224.2 84c.4.5.9 1 1.3 1.5z"></path>
                            <path d="M231.7 93.4l14.8-10.8c-.4-.5-.8-1.1-1.1-1.6l-14.8 10.8c.4.5.7 1 1.1 1.6z"></path>
                            <path d="M237.1 101.8l15.9-9.2c-.3-.6-.7-1.1-1-1.7l-15.9 9.2c.3.6.7 1.1 1 1.7z"></path>
                            <path d="M241.5 110.8l16.7-7.5c-.3-.6-.5-1.2-.8-1.8l-16.7 7.5c.3.6.6 1.2.8 1.8z"></path>
                            <path d="M245 120.1l17.4-5.7c-.2-.6-.4-1.2-.6-1.9l-17.4 5.7c.2.7.4 1.3.6 1.9z"></path>
                            <path d="M247.5 129.8l17.9-3.8c-.1-.7-.3-1.3-.4-1.9l-17.9 3.8c.1.6.3 1.3.4 1.9z"></path>
                            </svg>
                        </div>  
                        <div id="nums_2" class="nums">
							<?php if ( $aStats[2]['total'] < 100 ) : ?>
                            <div class="num">0</div>
                            <div class="num">20</div>
                            <div class="num">40</div>
                            <div class="num">60</div>
                            <div class="num">80</div>
                            <div class="num">100</div>
                            <?php elseif ( $aStats[2]['total'] < 200 ) : ?>
                            <div class="num">0</div>
                            <div class="num">40</div>
                            <div class="num">80</div>
                            <div class="num">120</div>
                            <div class="num">160</div>
                            <div class="num">200</div>
                            <?php else : ?>
                            <div class="num">0</div>
                            <div class="num">2000K</div>
                            <div class="num">4000K</div>
                            <div class="num">6000K</div>
                            <div class="num">8000K</div>
                            <div class="num">10000K</div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <span class="perc"><?= $aStats[2]['total']; ?></span><br>FTD
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( !empty($model) ) : ?>
<div id="overlay">
    <div id="text"><?= $model->agent->name . ' - ' . Yii::$app->cc->getCurrencies($model->currency) . number_format($model->amount, 2); ?></div>
</div>
<?php endif; ?>



<script type="text/javascript">

    showProgress();
    function showProgress() {
        $(".progress").each(function (key) {

            var $bar = $(this).find(".bar-wrap");
            var $val = parseInt($(this).find(".perc").html().replace(',', '')) * 100;

            if ( $(this).find(".perc").hasClass('total-price') ) {
                $val = $val / 1000;
            }

            var $total = parseInt($(this).find('.nums .num').last().html());

            $step = $val / $total;
            // alert($step);
            // if ( $step > 0 ) {
            //     console.log($step);
            //     console.log($total); 
            // }
            $(this).find(".bar-wrap").css({
                        transform: "rotate(" + ( 45 + $step * 1.8) + "deg)", // 100%=180° so: ° = % * 1.8
                        // 45 is to add the needed rotation to have the green borders at the bottom
            });
        });
    }

	<?php if ( empty($model) ) : ?>
	bReload = true;
	<?php else : ?>
	bReload = false;
	var audio = new Audio('<?= Url::to(['/here_comes_the_money.mp3']) ?>');
	audio.play();
	setTimeout(function() { 
		bReload = true; 
	}, 10000);
	<?php endif; ?>
</script>

<?php Pjax::end(); ?>








