<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'Account');
$this->params['breadcrumbs'][] = [
    'label' => $this->title, 
    'url' => ['/user/settings/account'], 
];

// echo '<pre>'; print_r($aProfile->bio); die;

?>

<section class="wrapper">
    <div class="container section-login-form">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <?= $this->render('//site/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                
            </div>
        </div>

        <div class="row dropbox-link-section">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <label><?= Yii::t('user', 'Google Drive URL'); ?></label>
                <input value="<?= $dropbox; ?>" disabled="" class="input-text form-control">
            </div>
        </div>

        <div class="row tabs-section">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="warpper">
                    <input class="radio" id="one" name="group" type="radio" checked="">
                    <input class="radio" id="two" name="group" type="radio">
                    <input class="radio" id="three" name="group" type="radio">
                    <div class="tabs">
                        <label class="tab" id="one-tab" for="one"><?= Yii::t('app', 'Change Password'); ?></label>
                        <label class="tab" id="two-tab" for="two"><?= Yii::t('app', 'Delete Account'); ?></label>
                    </div>
                    <div class="panels">
                        <div class="panel" id="one-panel">
                            <h3 class="panel-title"><?= Yii::t('app', 'Change Password'); ?></h3>
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'options' => [
                                    'class' => 'login-form'
                                ],
                                'fieldConfig' => [
                                    'template' => '{label}{input} <br />',
                                    'labelOptions' => [
                                        'class' => 'label'
                                    ],
                                ],
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                                'enableClientScript' => false,
                            ]); ?>

                                <?php if ($model->hasErrors()) : ?>
                                <div class="alert alert-danger">
                                    <?= $form->errorSummary($model); ?>
                                </div>
                                <?php endif; ?>

                                <div class="row">
                                    <div class="col-12">
                                        <?= $form->field($model, 'username', ['inputOptions' => ['class' => 'input-text form-control not-empty'], 'options' => ['class' => 'form-field']])->error(false) ?>
                                    </div>
                                    <div class="col-12">
                                        <?= $form->field($model, 'new_password', ['inputOptions' => ['class' => 'input-text form-control not-empty'], 'options' => ['class' => 'form-field']])->passwordInput()->error(false) ?>
                                    </div>
                                    <div class="col-12">
                                        <?= $form->field($model, 'current_password', ['inputOptions' => ['class' => 'input-text form-control not-empty '], 'options' => ['class' => 'form-field']])->passwordInput()->error(false) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-3 col-lg-9">
                                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'button button-primary']) ?><br>
                                    </div>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="panel" id="two-panel">
                            <h3 class="panel-title"><?= Yii::t('app', 'Delete account'); ?></h3>
                            <ul>
                                <li>* <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.</li>
                                <li>* <?= Yii::t('user', 'It will be deleted forever') ?>.</li>
                                <li>* <?= Yii::t('user', 'Please be certain') ?>.</li>
                            </ul>
                            <br />
                            <?= Html::a(Yii::t('user', 'Delete account'), ['delete'], [
                                'class' => 'btn btn-danger',
                                'data-confirm' => Yii::t('user', 'Are you sure? There is no going back'),
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>