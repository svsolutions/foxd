<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile');
$this->params['breadcrumbs'][] = [
    'label' => $this->title, 
    'url' => ['/user/settings/profile'], 
];

?>

    <div class="row account-settings-page">
        <div class="col-md-3 col-sm-4">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <?= $this->render('//site/_alert', ['module' => Yii::$app->getModule('user')]) ?>

                    <?php $form = ActiveForm::begin([
                        'id' => 'profile-form',
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                            'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnBlur' => false,
                        'enableClientScript' => false,
                    ]); ?>

                    <?= $form->field($model, 'name', ['inputOptions' => ['class' => 'input-text']])->label('Име') ?>

                    <?= $form->field($model, 'public_email', ['inputOptions' => ['class' => 'input-text']])->label('E-mail') ?>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <?= Html::submitButton(Yii::t('user', 'Запази'), ['class' => 'filter button btn']) ?>
                            <br>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
