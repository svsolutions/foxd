<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $form yii\widgets\ActiveForm
 */

$this->title = Yii::t('user', 'Networks');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//site/_banner'); ?>

<div class="page-content">                  
    <?= $this->render('//site/_breadcrumbs'); ?>

    <div class="row account-settings-page">

        <div class="col-md-3 col-sm-4">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= Html::encode($this->title) ?>
                </div>
                <div class="panel-body">
                    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

                    <div class="alert alert-info">
                        <p><?= Yii::t('user', 'You can connect multiple accounts to be able to log in using them') ?>.</p>
                    </div>
                    <?php $auth = Connect::begin([
                        'baseAuthUrl' => ['/user/security/auth'],
                        'accounts' => $user->accounts,
                        'autoRender' => false,
                        'popupMode' => false,
                    ]) ?>
                    <table class="table">
                        <?php foreach ($auth->getClients() as $client): ?>
                            <tr>
                                <td style="width: 32px; vertical-align: middle">
                                    <?= Html::tag('span', '', ['class' => 'auth-icon ' . $client->getName()]) ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <strong><?= $client->getTitle() ?></strong>
                                </td>
                                <td style="width: 120px">
                                    <?= $auth->isConnected($client) ?
                                        Html::a(Yii::t('user', 'Disconnect'), $auth->createClientUrl($client), [
                                            'class' => 'filter button btn',
                                            'data-method' => 'post',
                                        ]) :
                                        Html::a(Yii::t('user', 'Connect'), $auth->createClientUrl($client), [
                                            'class' => 'filter button btn',
                                        ])
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php Connect::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
