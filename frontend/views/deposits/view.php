<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Deposits */

$this->title = $model->agent->name . ' ' . Yii::$app->cc->getCurrencies($model->currency) . number_format($model->amount, 2);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deposits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deposits-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'brand_id',
                'value' => Yii::$app->cc->getBrands($model->brand_id)
            ],
            [
                'attribute' => 'agent_id',
                'value' => $model->agent->name
            ],
            [
                'attribute' => 'account_id',
                'value' => $model->account->name
            ],
            'deposit_date:datetime',
            [
                'attribute' => 'amount',
                'value' => Yii::$app->cc->getCurrencies($model->currency) . number_format($model->amount, 2)
            ],
            [
                'attribute' => 'payment_method',
                'value' => Yii::$app->cc->getPaymentMethods($model->payment_method)
            ],
            'notif:boolean'
        ],
    ]) ?>

</div>
