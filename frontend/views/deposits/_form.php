<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Deposits */
/* @var $form yii\widgets\ActiveForm */

if ( $model->isNewRecord ) {
    $model->deposit_date = date('Y-m-d H:i:s');
}

?>

<div class="deposits-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'agent_id', ['options' => ['class' => 'col-md-3']])->dropdownList(ArrayHelper::map(Yii::$app->cc->getAgents(), 'id', 'name'), ['prompt' => 'Select agent']) ?>

        <?= $form->field($model, 'brand_id', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getBrands(), ['prompt' => 'Select brand']) ?>

        <?= $form->field($model, 'account_id', ['options' => ['class' => 'col-md-3']])->dropdownList(ArrayHelper::map(Yii::$app->cc->getClients(), 'account_id', 'name'), ['prompt' => 'Select client']) ?>

        <?= $form->field($model, 'amount', ['options' => ['class' => 'col-md-2']])->textInput() ?>

        <?= $form->field($model, 'currency', ['options' => ['class' => 'col-md-1']])->dropdownList(Yii::$app->cc->getCurrencies()) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'payment_method', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getPaymentMethods(), ['prompt' => 'Select payment method']) ?>

        <?= $form->field($model, 'deposit_date')->hiddenInput()->label(false) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
