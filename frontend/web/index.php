<?php

if ( $_SERVER['REMOTE_ADDR'] != '154.49.102.36' && $_SERVER['REMOTE_ADDR'] != '85.11.165.137' && $_SERVER['REMOTE_ADDR'] != '::1' ) {
	echo "Forbidden access!"; die;
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('DEF_LANG') or define('DEF_LANG', 'en');

date_default_timezone_set('Europe/Sofia');

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../../common/config/main-local.php',
    require __DIR__ . '/../config/main.php',
    require __DIR__ . '/../config/main-local.php'
);

(new yii\web\Application($config))->run();
