<?php
return [
    'captcha' => [
    	'url' => 'https://www.google.com/recaptcha/api/siteverify',
    	'sitekey' => '6LctAEQUAAAAAHqgq3ItUjkz58Ddxpf6phCG59Xi',
    	'secret' => '6LctAEQUAAAAAILhUl_psynExaiWy-VNR8BWreHH'
    ],
    'api_test' => [
    	'otofield' => [
    		'url' => 'crm.otofield.com',
    		'token' => 'JDJ5JDEwJDdMa0cyVDZxc3pld'
    	]
    ],
	'api' => [
		'quantum' => [
            'brand_id' => '2', // Quantum
			'url'	=> 'crm.quantummarket.net',
			'token' => 'JDJ5JDEwJEIuenl6VlFIWXZXV'
		],
		'lifecoinfex' => [
            'brand_id' => '1', // LifecoinFX
			'url'	=> 'crm.lifecoinfex.net',
			'token' => 'JDJ5JDEwJEFtWk9pOTR4dkoya'
		]
	]
];
