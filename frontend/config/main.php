<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'language' => DEF_LANG,
    'sourceLanguage' => DEF_LANG,

    'id' => 'app-frontend',
    'name' => 'Foxd',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',

            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'admins' => ['scelov'],


            'mailer' => [
                    'sender'                => 'no-reply@myhost.com', // or ['no-reply@myhost.com' => 'Sender name']
                    'welcomeSubject'        => 'Welcome ',
                    'confirmationSubject'   => 'Confirmation account',
                    'reconfirmationSubject' => 'Email change',
                    'recoverySubject'       => 'Recovery academy',
            ],

            'controllerMap' => [
                'registration' => 'frontend\controllers\RegistrationController',
                'security' => 'frontend\controllers\SecurityController',
                'settings' => 'frontend\controllers\SettingsController',
                'profile' => 'frontend\controllers\ProfileController',
                'recovery' => 'frontend\controllers\RecoveryController',
                'admin' => 'frontend\controllers\AdminController',
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],
    ],
    'components' => [
        'session' => [
            'class' => 'yii\web\Session',
            'timeout' => 9999999999999,
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => false
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'class' => 'common\components\Request',
            'web'=> '/frontend/web'
        ],
        'user' => [
            // 'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            // 'autoRenewCookie' => true,
            // 'authTimeout' => 657567576,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            // 'name' => 'advanced-frontend',
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 7 * 24 *60 * 60]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        
    ],
    'params' => $params,
];
