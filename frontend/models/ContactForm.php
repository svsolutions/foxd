<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $subject;
    public $body;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email'], 'required'],
            [['body', 'phone'], 'safe'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            // ['verifyCode', 'captcha'],
        ];
    }

    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'E-mail'),
            'subject' => Yii::t('app', 'Subject'),
            'body' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject('Inquiry from ' . Yii::$app->name)
            ->setHtmlBody('
                <b>' . $this->attributeLabels()['name'] . ':</b> ' . $this->name . '<br/>
                <b>' . $this->attributeLabels()['email'] . ':</b> ' . $this->email . ' <br /><br /> 
                <b>' . $this->attributeLabels()['phone'] . ':</b> ' . $this->phone .' <br /> 

                <b>' . $this->attributeLabels()['body'] . ':</b> <br />' . $this->body)
            ->send();
    }
}
