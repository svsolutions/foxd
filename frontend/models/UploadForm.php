<?php 
namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;

use yii\imagine\Image;
use Imagine\Image\Box;  

class UploadForm extends Model {
    /**
     * @var UploadedFile
     */
    public $image;
    public $link;

    public function rules() {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => ['jpg', 'jpeg', 'svg', 'png']],
            [['link'], 'file', 'skipOnEmpty' => false, 'extensions' => ['doc']],
        ];
    }
}