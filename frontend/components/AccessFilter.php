<?php

namespace frontend\components;

use Yii;

/**
 * AccessFilter
 *
 * @author Marin Vasilev <m.vasilev@evoapp.bg>
 * @link <http://evoapp.bg> EvoApp Ltd. Official Website
 */
class AccessFilter extends \yii\filters\AccessRule
{
    /** @inheritdoc */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role === '?') {
                if (Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === 'member') {
                if (!Yii::$app->user->isGuest && Yii::$app->user->can('member')) {
                    return true;
                }
            }
        }

        return false;
    }
}