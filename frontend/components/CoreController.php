<?php
namespace frontend\components;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

/**
 * Core controller
 */
class CoreController extends Controller {
		
	public function __construct($id, $module, $config = []) {
	    parent::__construct($id, $module, $config);
	    
	    $aLangs = Yii::$app->params['languages'];

	    if ( isset($_GET['l']) ) {
	    	setcookie('lang', $_GET['l'], time() + 86400 * 365, '/');

	    	$strUrl = $_GET['l'] == DEF_LANG ? $this->redirect(Url::to(['/'])) : $this->redirect(Url::to(['/']) . $_GET['l']);

	    	return $strUrl;
	    }
	}

	public function getUserId() {
		return isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : false;
	}
}