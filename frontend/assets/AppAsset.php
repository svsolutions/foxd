<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'css/style.css',
    ];

    public $js = [
        'js/script.js?v=1.0.0.1',
    ];
    public $depends = [
    ];

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $strDir = __DIR__ . '/../web/';

        $aFiles = scandir($strDir . 'css');
        foreach ( $aFiles as $strFile ) {
            if ( strpos($strFile, '.css') !== FALSE ) {
                $this->css[] = 'css/' . $strFile;
            }
        }
        $aFiles = scandir($strDir . 'js');
        foreach ( $aFiles as $strFile ) {
            if ( strpos($strFile, '.js') !== FALSE && !in_array('js/' . $strFile, $this->js) ) {
                $this->js[] = 'js/' . $strFile;
            }
        }
    }   

}
