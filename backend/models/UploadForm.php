<?php 
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;

use yii\imagine\Image;
use Imagine\Image\Box;  

class UploadForm extends Model {
    /**
     * @var UploadedFile
     */
    public $image;

    public function rules() {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => ['jpg', 'jpeg', 'svg', 'png']],
        ];
    }
    
    public function upload($strFolder = '', $strKey = 'banners') {
        if ($this->validate()) {
            FileHelper::createDirectory(__DIR__ . '/../../frontend/web/uploads/' . $strFolder . '/', $mode = 0777, $recursive = true);

            $strImg = str_replace(' ', '_', $this->image->baseName) . '.' . $this->image->extension;
            $strPath = __DIR__ . '/../../frontend/web/uploads/' . $strFolder;

            $this->image->saveAs($strPath . $strImg);
            
            if ( strpos($strImg, '.svg') === FALSE) {

                foreach (\Yii::$app->params['image_sizes'][$strKey] as $size) {
                    Image::resize($strPath . $strImg, $size[0], $size[1])
                        ->save($strPath . 'size-' . $size[0] . 'x' . $size[1] . '_' . $this->clearName($strImg), 
                                ['quality' => 100]);
                }
            }

            return $strImg;
        } else {
            return false;
        }
    }

    private function clearName($strName) {
        return str_replace(["'"], '', $strName);
    }
}