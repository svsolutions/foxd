<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DepositsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Deposits & Withdrawals');
$this->params['breadcrumbs'][] = $this->title;

$fromDate = Yii::$app->request->get('from_date');
$toDate = Yii::$app->request->get('to_date');

?>
<div class="deposits-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-dashboard"></i> ' . Yii::t('app', 'Leaderboard'), 'https://foxd.info/', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Create Deposits'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-minus"></i> ' . Yii::t('app', 'Create Withdrawals'), ['create', 'type' => '2'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::a('<i class="glyphicon glyphicon-file"></i> ' . Yii::t('app', 'Export to CSV'), Yii::$app->request->url . '&export=1', ['class' => 'btn btn-info', 'style' => 'float: right; margin-bottom: 20px;']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [
            [
                'attribute' => 'account_id',
                'value' => function ($data) {
                    return isset($data->account->account_id) ? $data->account->account_id : null;
                },
            ],
            [
                'attribute' => 'brand_id',
                'value' => function ($data) {
                    return Yii::$app->cc->getBrands($data->brand_id);
                },
                'filter' => Yii::$app->cc->getBrands()
            ],
            [
                'attribute' => 'agent_id',
                'value' => function ($data) {
                    return $data->agent->name;
                },
                'filter' => ArrayHelper::map(Yii::$app->cc->getAgents(), 'id', 'name')
            ],
            // [
            //     'attribute' => 'account_id',
            //     'value' => function ($data) {
            //         return isset($model->account->name) ? $model->account->name : null;
            //     },
            //     'filter' => ArrayHelper::map(Yii::$app->cc->getClients(), 'id', 'name')
            // ],
            [
                'attribute' => 'deposit_date',
                'format' => 'date',
                'value' => 'deposit_date',
                'filter' => '<div class="row"><div class="col-md-6"><input type="date" value="' . $fromDate . '" name="from_date" class="form-control" /></div><div class="col-md-6"><input type="date" value="' . $toDate . '" name="to_date" class="form-control" /></div></div>'
            ],
            [
                'attribute' => 'amount',
                'value' => function ($data) {
                    return Yii::$app->cc->getCurrencies($data->currency) . number_format(abs($data->amount), 2);
                },
                'footer' => \common\models\Deposits::getTotal($dataProvider->query->all(), 'amount'),  
            ],
            [
                'attribute' => 'payment_method',
                'value' => function ($data) {
                    if ( empty($data->payment_method) ) {
                        return null;
                    }
                    return Yii::$app->cc->getPaymentMethods($data->payment_method);
                },
                'filter' => Yii::$app->cc->getPaymentMethods()
            ],
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    return Yii::$app->cc->getTypes($data->type);
                },
                'filter' => Yii::$app->cc->getTypes()
            ],
            [
                'attribute' => 'wd_type',
                'value' => function ($data) {
                    return Yii::$app->cc->getWTypes($data->wd_type, true);
                },
                'filter' => Yii::$app->cc->getWTypes()
            ],
            [
                'attribute' => 'department',
                'value' => function ($data) {
                    return Yii::$app->cc->getDepartments($data->agent->dep_id, true);
                },
                'filter' => Yii::$app->cc->getDepartments()
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
