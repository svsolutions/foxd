<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Deposits */
/* @var $form yii\widgets\ActiveForm */

$aAgents = [];
if ( $model->isNewRecord ) {
    $model->deposit_date = date('Y-m-d');
    $model->type = $type;
} else {
    $model->amount = abs($model->amount);
    $model->deposit_date = date('Y-m-d', strtotime($model->deposit_date));

    if ( !empty($model->agent_id) ) {
        $aAgents = Yii::$app->cc->getAgents($model->agent_id);
        if (!empty($aAgents) ) {
            $aTmp[$model->agent_id] = $aAgents->name;
            $model->department = $aAgents->dep_id;
            $aAgents = $aTmp;
        }
    }
}


?>

<div class="deposits-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'department', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getDepartments(), ['prompt' => 'Select department']) ?>

        <?= $form->field($model, 'brand_id', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getBrands(), ['prompt' => 'Select brand']) ?>

        <?= $form->field($model, 'agent_id', ['options' => ['class' => 'col-md-3']])->dropdownList($aAgents, ['prompt' => 'First select department and brand']) ?>

        <?php if ( true ) : ?>
                <div class="col-md-3">
                    <div class="field-deposits-account_id">
                        <label class="control-label" for="deposits-account_id-source">Account</label>
                        <input type="text" id="deposits-account_id-source" class="form-control" autocomplete="off" value="<?= isset($model->account->name) ? $model->account->name : '' ?>" placeholder="Select client" aria-invalid="true">
                        <?= $form->field($model, 'account_id', ['options' => ['class' => '']])->hiddenInput()->label(false) ?>
                    </div>
                    
                    <ul id="clients-holder"></ul>
                </div>
        <?php else : ?>
        <?= $form->field($model, 'account_id', ['options' => ['class' => 'col-md-3']])->dropdownList(ArrayHelper::map(Yii::$app->cc->getClients(), 'account_id', 'name'), ['prompt' => '']) ?>
        <?php endif; ?>

        <?= $form->field($model, 'amount', ['options' => ['class' => 'col-md-2']])->textInput() ?>

        <?= $form->field($model, 'currency', ['options' => ['class' => 'col-md-1']])->dropdownList(Yii::$app->cc->getCurrencies()) ?>
   
        <?= $form->field($model, 'payment_method', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getPaymentMethods(), ['prompt' => 'Select payment method']) ?>

        <?php if ( isset($_GET['type']) && $_GET['type'] == '2' || $model->type == 2 ) : ?>
        <?= $form->field($model, 'wd_type', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getWTypes(), ['prompt' => 'Select Withdrawal Type']) ?>
        <?php endif; ?>

        <?= $form->field($model, 'deposit_date', ['options' => ['class' => 'col-md-3']])->textInput(['type' => 'date']) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
