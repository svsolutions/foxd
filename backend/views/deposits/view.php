<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Deposits */

$this->title = $model->agent->name . ' ' . Yii::$app->cc->getCurrencies($model->currency) . number_format(abs($model->amount), 2);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deposits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deposits-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'type',
                'value' => Yii::$app->cc->getTypes($model->type)
            ],
            [
                'attribute' => 'brand_id',
                'value' => Yii::$app->cc->getBrands($model->brand_id)
            ],
            [
                'attribute' => 'agent_id',
                'value' => $model->agent->name
            ],
            [
                'attribute' => 'account_id',
                'value' => isset($model->account->name) ? $model->account->name : '-/-'
            ],
            'deposit_date:date',
            [
                'attribute' => 'amount',
                'value' => Yii::$app->cc->getCurrencies($model->currency) . number_format(abs($model->amount), 2)
            ],
            [
                'attribute' => 'payment_method',
                'value' => !empty($model->payment_method) ? Yii::$app->cc->getPaymentMethods($model->payment_method) : null
            ],
            [
                'attribute' => 'wd_type',
                'value' => Yii::$app->cc->getWTypes($model->wd_type, true)
            ],
            [
                'attribute' => 'user_id',
                'value' => !empty($model->user_id) ? $model->user->username : null
            ],
            'notif:boolean'
        ],
    ]) ?>

</div>
