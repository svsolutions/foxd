<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Deposits */

$this->title = $model->type == '1' ? Yii::t('app', 'Update Deposit') : Yii::t('app', 'Update Withdrawal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deposits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="deposits-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'type' => $model->type
    ]) ?>

</div>
