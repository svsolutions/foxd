<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AgentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agents-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agents'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'dep_id',
                'value' => function ($data) {
                    if ( empty($data->dep_id) ) return null;
                    return Yii::$app->cc->getDepartments($data->dep_id);
                },
                'filter' => Yii::$app->cc->getDepartments()
            ],
            [
                'attribute' => 'brand_id',
                'value' => function ($data) {
                    if ( empty($data->brand_id) ) return null;
                    return Yii::$app->cc->getBrands($data->brand_id);
                },
                'filter' => Yii::$app->cc->getBrands()
            ],
            'name',
            'active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
