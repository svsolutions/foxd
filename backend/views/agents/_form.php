<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Agents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agents-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
	    <?= $form->field($model, 'dep_id', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getDepartments(), ['prompt' => 'Select agent department']) ?>

	    <?= $form->field($model, 'brand_id', ['options' => ['class' => 'col-md-3']])->dropdownList(Yii::$app->cc->getBrands(), ['prompt' => 'Select brand']) ?>

	    <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-5']])->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropdownList(['1' => Yii::t('app', 'Yes'), '0' => Yii::t('app', 'No')]) ?>

	</div>
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end(); ?>

</div>
