<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
// echo '<pre>'; print_r($model); die;

$strLang = $model->lang->value;
if ( $strLang == 'es' ) {
    $strLang = '';
} else {
    $strLang = '/' . $strLang;
}

?>
<div class="pages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'View page'), Yii::$app->urlManagerFrontEnd->createUrl([$strLang . str_replace('/page//page/', '/page/', '/page/' . $model->slug)]), ['class' => 'btn btn-success' , 'target' => '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Remove'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Do you want to remove this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="row">
        <div class="col-md-6">
            <h4><?= Yii::t('app', 'Information'); ?></h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'lang',
                        'value' => Yii::$app->cc->getLanguages($model->lang->value)
                    ],
                    'slug',
                    // 'views',
                    'active:boolean',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if ( !empty($aTags) ) : ?>
            <h4><?= Yii::t('app', 'Meta'); ?></h4>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <?php foreach ( $aTags as $k => $pRow ) : ?>
                    <tr>
                        <th><?= $k; ?></th>
                        <td><?= $pRow; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>

    <p>
        <?php $model->content; ?>
    </p>

</div>
