<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
if ( $model->isNewRecord ) {
	$model->views = 0;
}

$this->registerJs(
    " 
    $('.newTag').unbind('click').bind('click', function() {
        $('.meta-template').clone().removeClass('hidden').removeClass('meta-template').appendTo('.tags-holder');
        });

        ",
    View::POS_READY,
    'pages-sctipt'
);
// echo '<pre>'; print_r($model['lang']->value); die;
if ( isset($model['lang']->value) )
    $model->langs = $model['lang']->value;

$model->left_menu = isset($aTags['left_menu']) ? $aTags['left_menu'] : 0;

?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-5']])->textInput(['maxlength' => true, 'class' => $model->isNewRecord ? 'slug-source form-control' : 'form-control']) ?>

        <?= $form->field($model, 'slug', ['options' => ['class' => 'col-md-3']])->textInput(['maxlength' => true, 'class' => $model->isNewRecord ? 'slug-field form-control' : 'form-control']) ?>

        <?= $form->field($model, 'langs', ['options' => ['class' => 'col-md-2']])->dropdownList(Yii::$app->cc->getLanguages()) ?>
        
        <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropdownList(['1' => Yii::t('app', 'Yes'), '0' => Yii::t('app', 'No')]) ?>
    </div>

    <div class="row">
        <?= Yii::$app->rc->getRelated(new \common\models\Pages, $model->id); ?>
    </div>

    <div class="row">
        <div class="col-md-12 tags-holder">
            <hr />
            <h4><?= Yii::t('app', 'Meta'); ?> <span class="newTag"><?= Yii::t('app', '+'); ?></span></h4>
            <br />
            <div class="row mt-20">
                <div class="col-md-4 field-news-title required hidden">
                    <label class="control-label"><?= Yii::t('app', 'SEO title'); ?></label>
                    <input type="readonly" class="form-control" value="title" name="Tags[][key]" maxlength="128" aria-required="true" placeholder="Example: meta title">
                </div>
                <div class="col-md-8 field-news-title required">
                    <label class="control-label"><?= Yii::t('app', 'SEO title'); ?></label>
                    <textarea type="text" class="form-control" name="Tags[][value]" maxlength="128" aria-required="true"><?= isset($aTags['title']) ? $aTags['title'] : ''; ?></textarea>
                </div>
            </div> 

            <div class="row mt-20">
                <div class="col-md-4 field-news-title required hidden">
                    <label class="control-label"><?= Yii::t('app', 'SEO description'); ?></label>
                    <input type="readonly" class="form-control" value="description" name="Tags[][key]" maxlength="128" aria-required="true" placeholder="Example: meta title">
                </div>
                <div class="col-md-8 field-news-title required">
                    <label class="control-label"><?= Yii::t('app', 'SEO description'); ?></label>
                    <textarea type="text" class="form-control" name="Tags[][value]" maxlength="128" aria-required="true"><?= isset($aTags['description']) ? $aTags['description'] : ''; ?></textarea>
                </div>
            </div> 
            
            <?php if ( isset($aTags) && !empty($aTags) && count($aTags) >= 1 ) : ?>
                <hr />
                <h4><?= Yii::t('app', 'Допълнителни мета тагове'); ?></h4>
                <?php foreach ( $aTags as $k => $strTag ) : ?>
                    <?php if ( $k != 'title' && $k != 'description' ) : ?>
                    <div class="row mt-20">
                        <div class="col-md-4 field-news-title required">
                            <label class="control-label"><?= Yii::t('app', 'Key'); ?></label>
                            <input type="text" class="form-control" value="<?= $k; ?>" name="Tags[][key]" maxlength="128" aria-required="true" placeholder="Example: meta title">
                        </div>
                        <div class="col-md-8 field-news-title required">
                            <label class="control-label"><?= Yii::t('app', 'Value'); ?></label>
                            <textarea type="text" class="form-control" name="Tags[][value]" maxlength="128" aria-required="true"><?= $strTag; ?></textarea>
                        </div>
                    </div>       
                    <?php endif; ?>         
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="row mt-20 hidden meta-template">
                <div class="col-md-4 field-news-title required">
                    <label class="control-label"><?= Yii::t('app', 'Key'); ?></label>
                    <input type="text" class="form-control" value="" name="Tags[][key]" maxlength="128" aria-required="true" placeholder="Example: meta title">
                </div>
                <div class="col-md-8 field-news-title required">
                    <label class="control-label"><?= Yii::t('app', 'Value'); ?></label>
                    <textarea type="text" class="form-control" value="" name="Tags[][value]" maxlength="128" aria-required="true"></textarea>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <?= $form->field($model, 'content')->widget(alexantr\tinymce\TinyMCE::className(), [
     'options' => ['rows' => 12],
        
     'clientOptions' => [
    //        'selector'=> "textarea",  // change this value according to your HTML
    //        'plugins'=> "codesample",
    //        'toolbar'=> "codesample",
         
    //        'theme' => "advanced",
         
         //set br for enter
         'force_br_newlines' => true,
         'force_p_newlines' => false,
         'forced_root_block' => '',
         'extended_valid_elements' => 'script[language|type|src]',
         
         
         'file_picker_callback' => new yii\web\JsExpression("function(cb, value, meta) {
             var input = document.createElement('input');
             input.setAttribute('type', 'file');
             input.setAttribute('accept', 'image/*');
             
             // Note: In modern browsers input[type=\"file\"] is functional without 
             // even adding it to the DOM, but that might not be the case in some older
             // or quirky browsers like IE, so you might want to add it to the DOM
             // just in case, and visually hide it. And do not forget do remove it
             // once you do not need it anymore.

             input.onchange = function() {
               var file = this.files[0];
               





                var fd = new FormData();
                var files = file;
                fd.append('image',files);
                fd.append('" . Yii::$app->request->csrfParam . "', '" . Yii::$app->request->csrfToken . "');

                $.ajax({
                    url: '" . Url::to(['/pages/upload-file']) . "',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response != 0){
                            cb(response, { title: file.name });

                        }else{
                            alert('file not uploaded');
                        }
                    },
                });
             };
             
             input.click();
            }"),
         'plugins' => [
             "advlist autolink lists link charmap print preview anchor",
             "searchreplace visualblocks code fullscreen",
             "insertdatetime media table contextmenu paste image imagetools"
         ],
         
         'menubar'=> ["insert"],
         'automatic_uploads' => true,
         'file_picker_types'=> 'image',
         
         'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image imageupload | fontselect | cut copy paste"
     ]
    ]); ?>

    <?= $form->field($model, 'views')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
