<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Clients */
/* @var $form yii\widgets\ActiveForm */
if ( $model->isNewRecord ) {
	$model->from_date = date('Y-m-d H:i:s');
}
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
	    <?= $form->field($model, 'account_id', ['options' => ['class' => 'col-md-3']])->textInput() ?>

	    <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-3']])->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropdownList(['1' => Yii::t('app', 'Yes'), '0' => Yii::t('app', 'No')]) ?>
	    
	    <?= $form->field($model, 'from_date')->hiddenInput()->label(false) ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
