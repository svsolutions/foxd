<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6, 'readonly' => true]) ?>

    <div class="row">
	    <?php foreach ( Yii::$app->cc->getLanguages() as $k => $strLang ) : if ( empty($k) ) $k = DEF_LANG; ?>
	    <div class="form-group col-md-6">
	    	<label><?= $strLang; ?></label>
	    	<textarea class="form-control" rows="6" name="Message[<?= $k; ?>]"><?= $model->getTranslation($k) ?></textarea>
	    </div>
	    <?php endforeach; ?>
	</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
