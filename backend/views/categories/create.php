<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = 'New';
$this->params['breadcrumbs'][] = ['label' => 'List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$nParent = Yii::$app->request->get('parent_id', false);
?>
<div class="categories-create">
	<?php if ( $nParent ) : ?>
    <p>
        <?= Html::a('Back', ['view', 'id' => $nParent], ['class' => 'btn btn-default']) ?>
    </p>
	<?php endif; ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
