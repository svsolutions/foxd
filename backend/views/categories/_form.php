<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?= $form->field($model, 'parent_id', ['options' => ['class' => 'col-md-6']])->dropdownList(ArrayHelper::map($model->getCategories(), 'id', 'title'), ['prompt' => '']) ?>

        <?= $form->field($model, 'type', ['options' => ['class' => 'col-md-6']])->dropdownList($model->getTypes()) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-6']])->textInput(['maxlength' => true, 'class' => 'slug-source form-control']) ?>

        <?= $form->field($model, 'slug', ['options' => ['class' => 'col-md-2']])->textInput(['maxlength' => true, 'class' => 'slug-field form-control']) ?>

        <?= $form->field($model, 'lang', ['options' => ['class' => 'col-md-2']])->dropdownList(Yii::$app->cc->getLanguages()) ?>        
    </div>
    <div class="row">
        <?= $form->field($model, 'content', ['options' => ['class' => 'col-md-12']])->textarea(['rows' => 4]) ?>
    </div>

    <div class="form-group row">
        <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropdownList(['1' => Yii::t('app', 'Да'), '0' => Yii::t('app', 'Не')]) ?>
    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>


    <?php ActiveForm::end(); ?>

</div>
