<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'List', 'url' => ['index', 'CategoriesSearch' => ['type' => $model->type]]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ( !empty($model->parent_id) ) : ?>
        <?= Html::a('Back to parent', ['view', 'id' => $model->parent_id], ['class' => 'btn btn-warning']) ?>
        <?php endif; ?>
        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'parent_id',
                'format' => 'raw',
                'value' => isset($model->parent->title) ? $model->parent->title : null,
            ],
            [
                'attribute' => 'lang',
                'format' => 'raw',
                'value' => !empty($model->lang) ? $model->lang : DEF_LANG
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => $model->getTypes($model->type),
            ],
            'slug',
            'active:boolean',
        ],
    ]) ?>
    
    <br />

    <h3>Childs "<?= $model->title; ?>"</h3>
    <p>
        <?= Html::a('New', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-floppy-open"></span> Save order', '', ['class' => 'btn btn-info save-sortable-list-order', 'style' => 'display: none;']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-striped table-bordered sortable-list', 'data-order-controller' => 'categories'],
        'rowOptions' => ['class' => 'sortable-item'],
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'lang',
                'format' => 'raw',
                'value' => function($data) { 
                    return !empty($data->lang) ? $data->lang : DEF_LANG;
                }
            ],
            'slug',
            //'image',
            //'content:ntext',
            //'position',
            'active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
