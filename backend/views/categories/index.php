<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu list';
$this->params['breadcrumbs'][] = $this->title;

$model = new \common\models\Categories();
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('New item', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-floppy-open"></span> Save order', '', ['class' => 'btn btn-info save-sortable-list-order', 'style' => 'display: none;']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-striped table-bordered sortable-list', 'data-order-controller' => 'categories'],
        'rowOptions' => ['class' => 'sortable-item'],
        'columns' => [
            // 'id',
            'title',

            [
                'attribute' => 'lang',
                'value' => function($data) { 
                    return Yii::$app->cc->getLanguages((empty($data->lang) ? DEF_LANG : $data->lang));
                },
                'filter' => false
            ],

            // 'slug',
            //'image',
            //'content:ntext',
            // 'position',
            'active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
