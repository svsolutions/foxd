<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'language' => DEF_LANG,
    'id' => 'app-backend',
    'name' => 'Dashboard Administration',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => '/deposits',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',

            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'admins' => ['scelov'],

            'mailer' => [
                    'sender'                => 'no-reply@myhost.com', // or ['no-reply@myhost.com' => 'Sender name']
                    'welcomeSubject'        => 'Welcome subject',
                    'confirmationSubject'   => 'Confirmation subject',
                    'reconfirmationSubject' => 'Email change subject',
                    'recoverySubject'       => 'Recovery subject',
            ],

            // 'controllerMap' => [
            //     'registration' => 'frontend\controllers\RegistrationController',
            //     'security' => 'frontend\controllers\SecurityController',
            //     'settings' => 'frontend\controllers\SettingsController',
            // ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],
    ],
    'components' => [
        // 'view' => [
        //     'theme' => [
        //         'pathMap' => [
        //             '@dektrium/user/views/admin' => '@app/views/admin'  // mapping for override the views dektrium with  your views 
        //         ],
        //     ],
        // ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];
