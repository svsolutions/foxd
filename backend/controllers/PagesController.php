<?php

namespace backend\controllers;

use Yii;
use common\models\Pages;
use common\models\Meta;
use common\models\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\components\AccessFilter;

use backend\models\UploadForm;
use yii\web\UploadedFile;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'upload-file' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUploadFile() {
        $strFiled = 'image';

        $uploadModel = new \frontend\models\UploadForm();
   
        $uploadModel->$strFiled = \yii\web\UploadedFile::getInstanceByName('image');

        $strPath = dirname(dirname(__DIR__)) . '/frontend/web/uploads/page/';
        \yii\helpers\FileHelper::createDirectory($strPath, $mode = 0777, $recursive = true);

        $uploadModel->$strFiled->saveAs($strPath . $uploadModel->$strFiled->name);
        return Yii::$app->params['domain'] . '/uploads/page/' . $uploadModel->$strFiled->name;
    }


    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {

        $aParams = Yii::$app->request->queryParams;
        $aParams['lang'] = Yii::$app->session->get('l', DEF_LANG);

        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search($aParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $aMeta = Meta::find()->where(['item_id' => $id, 'type' => 'pages_tags'])->all();
        $aTmp2 = [];
        foreach ( $aMeta as $pField ) {
            $aTmp2[$pField->meta_key] = $pField->value;
        }
        return $this->render('view', [
            'model' => Pages::find()->with(['lang'])->where(['id' => $id])->one(),
            'aTags' => $aTmp2
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveMeta($model->id);
            Yii::$app->rc->saveRelated($model->id, 'pages', $model->lang->value);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = Pages::find()->with(['lang'])->where(['id' => $id])->one(); // $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveMeta($model->id);
            Yii::$app->rc->saveRelated($model->id, 'pages', $model->lang->value);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $aMeta = Meta::find()->where(['item_id' => $id, 'type' => 'pages_tags'])->all();
        $aTmp2 = [];
        foreach ( $aMeta as $pField ) {
            $aTmp2[$pField->meta_key] = $pField->value;
        }

        return $this->render('update', [
            'model' => $model,
            'aTags' => $aTmp2,
        ]);
    }

    private function saveMeta($nPostId = false) {
        if ( $nPostId ) {
            $aPost = Yii::$app->request->post('Tags', false);

            $model = Meta::find()->where(['item_id' => $nPostId, 'type' => 'pages_lang'])->one();
            if ( empty($model) ) {
                $model = new Meta();
                $model->item_id = $nPostId;
                $model->meta_key = 'lang';
                $model->type = 'pages_lang';
            }

            $aLang = Yii::$app->request->post('Pages');
            if ( !empty($aLang['langs']) ) {
                $strLang = $aLang['langs'];
            } else {
                $strLang = DEF_LANG;
            }

            $model->value = $strLang;
            $model->save();

            if ( $aPost ) {
                $aMeta = Meta::deleteAll(['item_id' => $nPostId, 'type' => 'pages_tags']);
                for ( $i = 0; $i < count($aPost); $i += 2 ) {
                    if ( !empty($aPost[$i]['key']) && !empty($aPost[$i + 1]['value']) ) {
                        $strKey = $aPost[$i]['key'];
                        $strVal = $aPost[$i + 1]['value'];
                        $model = new Meta();
                        $model->item_id = $nPostId;
                        $model->meta_key = $strKey;
                        $model->value = $strVal;
                        $model->type = 'pages_tags';
                        $model->save();
                    }
                }
            }
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
