<?php

namespace backend\controllers;

use Yii;
use common\models\Deposits;
use common\models\DepositsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DepositsController implements the CRUD actions for Deposits model.
 */
class DepositsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    
                    'class' => \backend\components\AccessFilter::className()
                    
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ]
            ]
        ];
    }

    /**
     * Lists all Deposits models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DepositsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // echo '<pre>'; print_r($_GET); die;
        if ( isset($_GET['export']) ) {
            $model = new Deposits;
            $fp = fopen('deposit_export.csv', 'w');

            fputcsv($fp, [
                $model->getAttributeLabel('account_id'),
                $model->getAttributeLabel('brand_id'),
                $model->getAttributeLabel('agent_id'),
                $model->getAttributeLabel('deposit_date'),
                $model->getAttributeLabel('amount'),
                $model->getAttributeLabel('payment_method'),
                $model->getAttributeLabel('type'),
                $model->getAttributeLabel('wd_type'),
                $model->getAttributeLabel('department')
            ]);

            foreach ($dataProvider->query->all() as $data) {
                fputcsv($fp, [
                    $data->account_id,
                    ( !empty($data->brand_id) ? Yii::$app->cc->getBrands($data->brand_id) : null ),
                    ( !empty($data->agent_id) ? $data->agent->name : null ),
                    date('Y-m-d', strtotime($data->deposit_date)),
                    Yii::$app->cc->getCurrencies($data->currency) . number_format(abs($data->amount), 2),
                    ( !empty($data->payment_method) ? Yii::$app->cc->getPaymentMethods($data->payment_method) : null ),
                    ( !empty($data->type) ? Yii::$app->cc->getTypes($data->type) : null ),
                    ( !empty($data->wd_type) ? Yii::$app->cc->getWTypes($data->wd_type, true) : null ),
                    ( !empty($data->agent_id) ? Yii::$app->cc->getDepartments($data->agent->dep_id, true) : null )
                ]);
            }
            fclose($fp);

            return $this->redirect(['/deposit_export.csv']);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Deposits model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Deposits model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Deposits();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->identity->id;
            if ( $model->save() ) {
                return $this->redirect(['create']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'type' => Yii::$app->request->get('type', '1')
        ]);
    }

    /**
     * Updates an existing Deposits model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->user_id = Yii::$app->user->identity->id;
            if ( $model->save() ) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Deposits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Deposits model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deposits the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deposits::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
