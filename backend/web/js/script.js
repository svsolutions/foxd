$(document).ready(function() {

	var nBrandId = '';

	jQuery('#deposits-brand_id').change(function() {
		var nID = jQuery(this).val();
		var dep = jQuery('#deposits-department').val();

		jQuery.get(strUrl + "agents/get-agents?id=" + dep + "&brand=" + nID, function( data ) {
    			var pObj = jQuery.parseJSON( data );
    			console.log(pObj);
    			jQuery('#deposits-agent_id').html('');
    			
				Object.keys(pObj).forEach(function(key) {
				    jQuery('#deposits-agent_id').append('<option value="' + key + '">' + pObj[key] + '</option>');
				});

    			// jQuery(pObj).each(function(k, v) {
    			// 	console.log(v);
    			// 	jQuery('#deposits-agent_id').append('<option value="' + k + '">' + v + '</option>');
    			// });
			});
	});

	jQuery('#deposits-brand_id').change(function() {
		nBrandId = jQuery(this).val();
		if ( nBrandId == '3' ) {
			nBrandId = '1';
		} else if ( nBrandId == '4' ) {
			nBrandId = '2';
		}
		console.log(nBrandId);
	});

    jQuery('#deposits-account_id-source').keyup(function() {
    	setVisibles(jQuery(this));
    });

    function setVisibles(pObj) {
		var strVal = jQuery(pObj).val();
    	if ( strVal.length > 2 ) {
    		jQuery('#clients-holder').hide();
    		jQuery.get(strUrl + "clients/clients?str=" + strVal + "&b=" + nBrandId, function( data ) {
    			var pObj = jQuery.parseJSON( data );
    			jQuery('#clients-holder').html('');
    			jQuery(pObj).each(function(k, v) {
    				jQuery('#clients-holder').show();
    				jQuery('#clients-holder').append('<li data-id="' + v.k + '" data-brand="' + v.b + '">' + v.k + ' ' + v.name + '</li>')
    			});
    			bindEvents();
			});



	    	// jQuery('#clients-holder li').hide();
	    	// jQuery('#clients-holder li').each(function() {
	    	// 	if ( jQuery(this).html().indexOf(strVal) >= 0 && ( nBrandId == jQuery(this).data('brand') ) ) {
	    	// 		jQuery(this).show();
	    	// 	}
	    	// });
	    	// jQuery('#clients-holder').show();
	    } else {
	    	jQuery('#clients-holder').hide();
	    }
    }

    function bindEvents() {
    	jQuery('#clients-holder li').bind('click', function() {
	    	var nID = jQuery(this).data('id');
	    	jQuery('#deposits-account_id').val(nID);
	    	jQuery('#deposits-account_id-source').val(jQuery(this).html());
	    	jQuery('#clients-holder').hide();
	    });
    }

});